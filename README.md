# flutter_mvi_counter

An example of how you can use the MVI architecture from Cycle.js with Flutter.

## Basic Idea

  * Model: A `Stream` to manage View State.
  * View: The Widget layer. Visual representation of the Model.
  * Intent: Interpret `Widget` events as user actions.
  
## Model

The Model layer is just a `Stream<ViewState>`. This makes it easy to test and reuse. It takes in Streams of actions (from the Intent or Firebase Database or Redux store) and combines / converts those into View State, which is consumed by the View.

## View
 
It takes in a `Stream<ViewState>` (aka the `Model`) and an `Intent` and renders a Widget! It should be concerned only with rendering a UI, no business logic included!

## Intent

A class that expresses the actions a user can take. It acts as the Glue between the View and Model.

## Testing

You can view tests in the `tests` folder. It aims to show why this pattern makes testing a breeze.

The tests can be executed using `flutter test` from the command line, or from within IntelliJ using the Flutter plugin.

## Dependencies

  * `flutter_stream_friends` for use in the `Intent` layer
  * `rxdart` for use in the Model layer